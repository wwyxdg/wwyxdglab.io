//Typed.js
var typed = new Typed('#hi', {
    strings: [
        "咸鱼.",
        "from 合肥, China.",
        "a student.",
        "good at fish.",
        "......?"
    ],
    typeSpeed: 100,
    loop: true,
});

//Nav links
function goHome() {
    $("#home").addClass("nav-active");
    $("#resume").removeClass("nav-active");
    $("#projects").removeClass("nav-active");
    $("#homeの").show();
    $("#resumeの").hide();
    $("#projectsの").hide();
}

function goResume() {
    $("#resume").addClass("nav-active");
    $("#home").removeClass("nav-active");
    $("#projects").removeClass("nav-active");
    $("#resumeの").show();
    $("#homeの").hide();
    $("#projectsの").hide();


}

function goProjects() {
    $("#projects").addClass("nav-active");
    $("#resume").removeClass("nav-active");
    $("#home").removeClass("nav-active");
    $("#homeの").hide();
    $("#resumeの").hide();
    $("#projectsの").show();

}


